///////////////////////
/// Hyperparameters ///
///////////////////////


#ifndef HYPERPARAMS_H
#define HYPERPARAMS_H


#define MAX_ERROR 3

#define DELTA_Z0_W 1.
#define DELTA_H0_W .5
#define DELTA_ZB_W 1.
#define DELTA_HB_W .5
#define DELTA_A_W 1.5

#define T_SEARCH_DEPTH 25
#define T_SEARCH_MIN 10000
#define T_SEARCH_MAX 300000

#define FLASH_SEARCH_DEPTH 45
#define FLASH_SEARCH_N 50
#define z0_min 0.
#define z0_max 100000.

#define TRAJ_SEARCH_DEPTH 55


#endif
